package com.arun.aashu.mywebview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        wv=findViewById(R.id.mywebview);

       // wv.getSettings().setJavaScriptEnabled(true);
        //or

        WebSettings ws=wv.getSettings();
        ws.setJavaScriptEnabled(true);

        wv.setWebViewClient(new MyWebClient());
        wv.loadUrl("http://www.ducatindia.com");

    }

    class MyWebClient extends WebViewClient{

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return(false);
        }
    }
}
